#!/usr/bin/env node
const moment = require('moment');
const storage = require('node-persist');
const co = require('co');
const prompt = require('co-prompt');
const chalk = require('chalk');

const options = {
  barScale: 2,
  dateFormat: 'YYYY-MM-DD'
};

const MAX_AGE = process.argv[2] || 78;
const NOW = moment();

const birthdate = moment('1987-05-09');
const at75Date = birthdate.add(MAX_AGE, 'years');

const daysFromBirth = NOW.diff(birthdate, 'days');
const daysTo75 = at75Date.diff(NOW, 'days');
const weeksTo75 = at75Date.diff(NOW, 'weeks');
const monthsTo75 = at75Date.diff(NOW, 'months');

const percentageLeft = (daysTo75 * 100 / (365 * MAX_AGE)).toFixed(3);

const mainMessage = `\n ${chalk.green('One less 🙌')}. Until you're ${MAX_AGE} years old, there are ${chalk.red(daysTo75)} days, ${chalk.red(weeksTo75)} weeks, ${chalk.red(monthsTo75)} months left.
 You have ${chalk.green(percentageLeft)}% of your life yet to live. \n`;

const graphTimeLeft = per => {
  return Array
    .apply(null, { length: 100 / options.barScale })
    .map(Number.call, Number)
    .map(n => n < (per) / options.barScale ? '❤️' : '🖤')
    .join('');
}

const showMessage = msg => {
  console.log(msg);
};

const setDOB = (error) => {
  if (error) showMessage(`🙅🏻 Sorry, that's not a valid date format! Try again?`);
  co(function *() {
    const dob = yield prompt('📅 Enter your birth date (year-mm-dd): ');
    const dateValid = moment(dob, options.dateFormat, true).isValid();
    storage
      .init()
      .then(() =>
        dateValid ? storage.setItem('dob', dob).then(showStats) : setDOB('error')
      )
  });
};

const showStats = () => {
  showMessage(`${mainMessage} \n ${graphTimeLeft(percentageLeft)} \n`)
  process.exit(1);
};

function initialise() {
  storage.init().then(() =>
    storage.getItem('dob')
      //.then(() => setDOB())
      .then((value) => value ? showStats() : setDOB())
      .catch(err => console.log(dob))
  );
}

initialise();
